# Fly Wherever
![icon](Images/Icon.png)

## Task Description

Create an app that includes 2 screens:

1) Airport selection screen with city name text field
   - UI design is up to developer
   - [Backend API sample](http://places.aviasales.ru/places?term=paris&locale=en)

2) Airplane flight animation screen, from St. Petersburg to the selected airport or city
   - The airplane path should be a geodesic line. In addition whould be good to have the shape of the path in accordance with the reference. Regarding everything else, the final design should be also close to the reference.
   - The airplane should fly smoothly (also when manipulating the map)
   - When map is shifted the airplane and the path should not shift relative to the map
   - As an airplane icon you must use the attached plane.pdf

Implement app only with Swift.

![flight path](Images/flight.png)

## Result

### Core

- Swift 5
- Protocol Oriented
- MVC architecture with Data and Network services
- Storyboard segue navigation
- Grand Central Dispatch
- Cancelable DispatchWorkItems

### REST API

- URLSession
- Codable
- Cancelable URLSessionTasks

### UI

- UIKit
- MapKit
- Interface Builder
- Auto Layouts
- Multiple device orientations
- iOS 13 Dark Appearance

||||
|-|-|-|
|![Enter destination](Images/1.png)|![List of destinations](Images/2.png)|![Flight](Images/3.png)|
