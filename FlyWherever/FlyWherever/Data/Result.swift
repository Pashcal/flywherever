//
//  Result.swift
//  FlyWherever
//
//  Created by Pavel Chupryna + on 20.04.2020.
//  Copyright © 2020 pashcal. All rights reserved.
//

import Foundation

enum Result<T> {
    case success(T)
    case failure(FlyError)
    
    var value: T? {
        switch self {
        case .success(let value):
            return value
        default:
            return nil
        }
    }
    
    var error: FlyError? {
        switch self {
        case .failure(let error):
            return error
        default:
            return nil
        }
    }
}
