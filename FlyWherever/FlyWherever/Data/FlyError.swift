//
//  FlyError.swift
//  FlyWherever
//
//  Created by Pavel Chupryna + on 20.04.2020.
//  Copyright © 2020 pashcal. All rights reserved.
//

import Foundation

enum FlyError: Error {
    case noInternetConnection
    case another(_ error: Error)
}
