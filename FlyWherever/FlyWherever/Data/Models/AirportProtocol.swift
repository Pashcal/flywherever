//
//  AirportProtocol.swift
//  FlyWherever
//
//  Created by Pavel Chupryna + on 16.04.2020.
//  Copyright © 2020 pashcal. All rights reserved.
//

import Foundation

protocol DestinationProtocol {
    var code: String { get }
    var airportName: String? { get }
    var cityName: String { get }
    var location: LocationProtocol { get }
}
