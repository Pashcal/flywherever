//
//  Location.swift
//  FlyWherever
//
//  Created by Pavel Chupryna + on 16.04.2020.
//  Copyright © 2020 pashcal. All rights reserved.
//

import Foundation

class Location: Equatable {
    
    let latitude: Double
    let longitude: Double
    
    init(lat: Double, lon: Double) {
        latitude = lat
        longitude = lon
    }
    
    static func == (lhs: Location, rhs: Location) -> Bool {
        let delta = 0.000001
        return (lhs.latitude - rhs.latitude).magnitude < delta
            && (lhs.longitude - rhs.longitude).magnitude < delta
    }
}
