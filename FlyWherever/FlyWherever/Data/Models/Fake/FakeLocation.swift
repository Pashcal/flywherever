//
//  FakeLocation.swift
//  FlyWherever
//
//  Created by Pavel Chupryna + on 16.04.2020.
//  Copyright © 2020 pashcal. All rights reserved.
//

import Foundation

class Location: LocationProtocol {
    
    let latitude: Double
    let longitude: Double
    
    init(lat: Double, lon: Double) {
        latitude = lat
        longitude = lon
    }
}
