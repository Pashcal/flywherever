//
//  Destination.swift
//  FlyWherever
//
//  Created by Pavel Chupryna + on 16.04.2020.
//  Copyright © 2020 pashcal. All rights reserved.
//

import Foundation

class Destination: CustomStringConvertible, Equatable {

    let location: Location
    let code: String
    let cityName: String
    let airportName: String?
    
    var name: String {
        return airportName ?? cityName
    }
    
    init(location: Location, code: String, cityName: String, airportName: String? = nil) {
        self.location = location
        self.code = code
        self.cityName = cityName
        self.airportName = airportName
    }
    
    var description: String {
        return "\(code)-\(cityName)-\(airportName ?? "")".trimmingCharacters(in: ["-"])
    }
    
    static func == (lhs: Destination, rhs: Destination) -> Bool {
        return lhs.location == rhs.location
            && lhs.code == rhs.code
            && lhs.cityName == rhs.cityName
            && lhs.airportName == rhs.airportName
    }
}
