//
//  Route.swift
//  FlyWherever
//
//  Created by Pavel Chupryna + on 18.04.2020.
//  Copyright © 2020 pashcal. All rights reserved.
//

import Foundation

class Route: CustomStringConvertible, Equatable {
    
    let from: Destination
    let to: Destination
    
    init(from: Destination, to: Destination) {
        self.from = from
        self.to = to
    }
    
    var description: String {
        return "[\(from) → \(to)]"
    }
    
    static func == (lhs: Route, rhs: Route) -> Bool {
        return lhs.from == rhs.from && lhs.to == rhs.to
    }
}
