//
//  DataService.swift
//  FlyWherever
//
//  Created by Pavel Chupryna + on 16.04.2020.
//  Copyright © 2020 pashcal. All rights reserved.
//

import Foundation

class DataService: DataServiceProtocol {
    
    let departure = Destination(location: Location(lat: 59.806084, lon: 30.3083),
                                code: "LED",
                                cityName: "Saint Petersburg, Russia",
                                airportName: "Pulkovo Airport")
    
    var networkTask: URLSessionDataTask?
    var workItem: DispatchWorkItem?
    
    lazy var networkService: NetworkService = {
        return NetworkService()
    }()
    
    func cancelSearch() {
        networkTask?.cancel()
        workItem?.cancel()
    }
    
    func searchDestination(query: String, completion: @escaping (Result<[Destination]>) -> Void) -> DispatchWorkItem {
        cancelSearch()
        let workItem = DispatchWorkItem {
            self.networkTask = self.networkService.getPlaces(term: query, locale: AppConfig.defaultLocale) { response in
                switch response {
                case .success(let places):
                    let destinations = places.map { (place) -> Destination in
                        let location = Location(lat: place.location.lat, lon: place.location.lon)
                        let destination = Destination(location: location,
                                                      code: place.iata,
                                                      cityName: place.name,
                                                      airportName: place.airportName)
                        return destination
                    }
                    completion(Result<[Destination]>.success(destinations))
                case .failure(let error):
                    print("ERROR | Data: \(error.localizedDescription)")
                    completion(Result<[Destination]>.failure(error))
                }
            }
        }
        self.workItem = workItem
        DispatchQueue.global(qos: .userInitiated).async(execute: workItem)
        return workItem
    }
}
