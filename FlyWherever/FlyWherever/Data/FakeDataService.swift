//
//  FakeDataService.swift
//  FlyWherever
//
//  Created by Pavel Chupryna + on 16.04.2020.
//  Copyright © 2020 pashcal. All rights reserved.
//

import Foundation

class FakeDataService: DataServiceProtocol {
    
    let departure = Destination(location: Location(lat: 59.806084, lon: 30.3083),
                                code: "LED",
                                cityName: "Saint Petersburg, Russia",
                                airportName: "Pulkovo Airport")
    
    let letters = Array("ABCDEFGHIJKLMNOPQRSTUVWXYZ")
    let fakeLocation = Location(lat: 13.7234186, lon: 100.4762319)
    
    var workItem: DispatchWorkItem?
    
    func cancelSearch() {
        workItem?.cancel()
    }
    
    func searchDestination(query: String, completion: @escaping (Result<[Destination]>) -> Void) -> DispatchWorkItem {
        cancelSearch()
        let workItem = DispatchWorkItem {
            var result = [Destination]()
            for letter in self.letters {
                let destination = Destination(location: self.fakeLocation,
                                              code: "BKK",
                                              cityName: query,
                                              airportName: letter == "A" ? nil : "\(query)_\(letter)")
                result.append(destination)
            }
            completion(Result<[Destination]>.success(result))
        }
        self.workItem = workItem
        DispatchQueue.global(qos: .userInitiated).asyncAfter(deadline: .now() + 0.5, execute: workItem)
        return workItem
    }
}
