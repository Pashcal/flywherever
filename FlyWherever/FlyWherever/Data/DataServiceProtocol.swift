//
//  DataServiceProtocol.swift
//  FlyWherever
//
//  Created by Pavel Chupryna + on 16.04.2020.
//  Copyright © 2020 pashcal. All rights reserved.
//

import Foundation

protocol DataServiceProtocol {
    var departure: Destination { get }
    func cancelSearch()
    func searchDestination(query: String, completion: @escaping (Result<[Destination]>) -> Void) -> DispatchWorkItem
}
