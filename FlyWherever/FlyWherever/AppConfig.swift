//
//  AppConfig.swift
//  FlyWherever
//
//  Created by Pavel Chupryna + on 17.04.2020.
//  Copyright © 2020 pashcal. All rights reserved.
//

import Foundation

class AppConfig {
    
    static var isUseFakeData: Bool = {
        return ProcessInfo.processInfo.arguments.contains("-FAKE")
    }()
    
    static let defaultLocale = "en"
    static let isGeodesicFlight = true
    static let isEmojiPlane = true
    
    static var isFun = false
}
