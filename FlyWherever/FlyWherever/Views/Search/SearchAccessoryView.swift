//
//  SearchAccessoryView.swift
//  FlyWherever
//
//  Created by Pavel Chupryna + on 20.04.2020.
//  Copyright © 2020 pashcal. All rights reserved.
//

import UIKit

class SearchAccessoryView: UIView {
    
    private let impactFeedbackgenerator = UIImpactFeedbackGenerator(style: .light)
    private var currentSearchState: SearchState?
    private var currentAccessoryState: SearchAccessoryState?
    
    @IBOutlet private weak var flyButton: UIButton!
    @IBOutlet private weak var statusLabel: UILabel!
    
    var destinationTextField: UITextField?
    var selectAction: (() -> Void)?
    
    func updateState(_ state: SearchState) {
        guard currentSearchState != state else {
            return
        }
        currentSearchState = state

        switch state {
        case .noInternetConnection:
            updateAccessoryState(.status("⚠️ No internet connection"))
        case .noResults:
            updateAccessoryState(.status("No results ¯\\_(ツ)_/¯\nTry another destination"))
        case .noDestination:
            updateAccessoryState(.empty)
        case .loading:
            updateAccessoryState(.loading)
        case .results(_):
            updateAccessoryState(.empty)
        case .selected(let destination):
            updateAccessoryState(.flyTo(destination))
            flyButton.setTitle("Fly to \(destination.code) ✈️", for: .normal)
            impactFeedbackgenerator.prepare()
        }
    }
    
    private func updateAccessoryState(_ state: SearchAccessoryState) {
        guard currentAccessoryState != state else {
            return
        }
        currentAccessoryState = state
        isUserInteractionEnabled = false
        // FIXME: Unknown glitch if try to scroll keyboard with tap on status label
        
        switch state {
        case .empty:
            flyButton.animateHidden(true)
            statusLabel.animateHidden(true)
        case .loading:
            flyButton.animateHidden(true)
            statusLabel.animateHidden(false)
            loadingDots = ""
            animateLoading()
        case .status(let text):
            flyButton.animateHidden(true)
            statusLabel.animateHidden(false)
            statusLabel.text = text
        case .flyTo(let destination):
            isUserInteractionEnabled = true
            flyButton.setTitle("Fly to \(destination.code) ✈️", for: .normal)
            flyButton.animateHidden(false)
            statusLabel.animateHidden(true)
            impactFeedbackgenerator.prepare()
        }
    }
    
    private var loadingDots = ""
    @objc private func animateLoading() {
        guard case .loading = currentAccessoryState else {
            return
        }
        
        loadingDots = loadingDots.count > 2 ? "" : loadingDots + "."
        statusLabel.text = "Loading\(loadingDots)"
        
        perform(#selector(animateLoading), with: nil, afterDelay: 0.8)
    }
    
    @IBAction private func flyAction(_ sender: Any) {
        switch currentSearchState {
        case .noDestination:
            destinationTextField?.becomeFirstResponder()
        case .selected(_):
            impactFeedbackgenerator.prepare()
            impactFeedbackgenerator.impactOccurred()
            selectAction?()
        default:
            break
        }
    }
    
    override func hitTest(_ point: CGPoint, with event: UIEvent?) -> UIView? {
        let view = super.hitTest(point, with: event)
        if view == self {
            return nil
        } else {
            return view
        }
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        
        let viewPath = UIBezierPath(roundedRect: flyButton.bounds, cornerRadius: flyButton.layer.cornerRadius).cgPath
        flyButton.layer.shadowPath = viewPath
        flyButton.layer.shadowColor = UIColor.systemBackground.cgColor
        flyButton.layer.shadowOffset = CGSize(width: 0, height: 0)
        flyButton.layer.shadowOpacity = 0.5
        flyButton.layer.shadowRadius = 8
    }
    
    override var intrinsicContentSize: CGSize {
        return CGSize.zero
    }
}
