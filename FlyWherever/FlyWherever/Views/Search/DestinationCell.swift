//
//  DestinationCell.swift
//  FlyWherever
//
//  Created by Pavel Chupryna + on 17.04.2020.
//  Copyright © 2020 pashcal. All rights reserved.
//

import UIKit

class DestinationCell: UITableViewCell {
    
    @IBOutlet private weak var codeLabel: UILabel!
    @IBOutlet private weak var nameLabel: UILabel!
    
    func setup(withDestination destination: Destination) {
        codeLabel.text = destination.code
        nameLabel.text = destination.name
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        let view = UIView()
        view.backgroundColor = UIColor.systemTeal
        self.selectedBackgroundView = view
        
        let titleFont = UIFont.preferredFont(forTextStyle: .title2)
        codeLabel.font = UIFont.monospacedSystemFont(ofSize: titleFont.pointSize, weight: .regular)
    }
}
