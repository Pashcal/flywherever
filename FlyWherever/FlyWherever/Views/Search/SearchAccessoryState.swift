//
//  SearchAccessoryState.swift
//  FlyWherever
//
//  Created by Pavel Chupryna + on 20.04.2020.
//  Copyright © 2020 pashcal. All rights reserved.
//

import Foundation

enum SearchAccessoryState: Equatable {
    case empty
    case loading
    case status(_ text: String)
    case flyTo(_ destination: Destination)
}
