//
//  SearchState.swift
//  FlyWherever
//
//  Created by Pavel Chupryna + on 18.04.2020.
//  Copyright © 2020 pashcal. All rights reserved.
//

import Foundation

enum SearchState: Equatable {
    case noInternetConnection
    case noDestination
    case loading
    case noResults
    case results(destinations: [Destination])
    case selected(destination: Destination)
}
