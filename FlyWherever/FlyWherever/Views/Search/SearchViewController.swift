//
//  SearchViewController.swift
//  FlyWherever
//
//  Created by Pavel Chupryna + on 16.04.2020.
//  Copyright © 2020 pashcal. All rights reserved.
//

import UIKit

class SearchViewController: UIViewController {
    
    private let impactFeedbackgenerator = UIImpactFeedbackGenerator(style: .light)
    
    @IBOutlet private weak var destinationTextField: UITextField!
    @IBOutlet private weak var tableView: UITableView!
    
    private var accessoryView: SearchAccessoryView!
    
    lazy private var dataService: DataServiceProtocol = {
        if AppConfig.isUseFakeData {
            return FakeDataService()
        } else {
            return DataService()
        }
    }()
    
    private var dataSourceDestinations = [Destination]()
    private var selectedDestination: Destination?
    
    private var currentState: SearchState?
    private func updateState(_ state: SearchState) {
        guard currentState != state else {
            return
        }
        currentState = state
        selectedDestination = nil
        
        switch state {
        case .results(let destinations):
            dataSourceDestinations = destinations
            tableView.animateReloadData()
        case .selected(let destination):
            selectedDestination = destination
            destinationTextField.resignFirstResponder()
            destinationTextField.text = destination.name
            impactFeedbackgenerator.prepare()
        default:
            dataSourceDestinations = []
            tableView.animateHidden(true) {
                self.tableView.reloadData()
                self.tableView.isHidden = false // To use swipe for dismiss keyboard
            }
            break
        }
        
        accessoryView.updateState(state)
    }
    
    private func fakeSetup() {
        let fakeDestination = "Bangkok"
        destinationTextField.text = fakeDestination
        _ = self.dataService.searchDestination(query: fakeDestination) { result in
            guard let destinations = result.value else {
                return
            }
            DispatchQueue.main.async {
                self.updateState(destinations.isEmpty ? .noResults : .results(destinations: destinations))
            }
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                self.tableView.selectRow(at: IndexPath(row: 0, section: 0), animated: true, scrollPosition: .none)
                self.updateState(.selected(destination: self.dataSourceDestinations[0]))
            }
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        accessoryView = Bundle.main.loadNibNamed("SearchAccessoryView", owner: nil, options: nil)?.first as? SearchAccessoryView
        accessoryView.destinationTextField = destinationTextField
        accessoryView.selectAction = { self.performSegue(withIdentifier: "fly", sender: self) }
        
        if AppConfig.isUseFakeData {
            fakeSetup()
        }
        
        updateState(.noDestination)
        destinationTextField.becomeFirstResponder()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // In case of using accessoryView notifications of keyboardWillHide is useless, becase after it's invoking keyboardDidShowNotification again with just accessoryView frame
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShown(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
    }
    
    private let accessoryViewHeight: CGFloat = 60 + 32 // Search button + margins
    
    @objc private func keyboardWillShown(notification: NSNotification) {
        guard let userInfo = notification.userInfo,
            let keyboardFrame = userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? CGRect else {
                return
        }
        
        var selectedCellWasVisible = false
        if let selectedRow = tableView.indexPathForSelectedRow {
            selectedCellWasVisible = tableView.indexPathsForVisibleRows?.contains(selectedRow) ?? false
        }
        
        // keyboardFrame also includes accessoryView frame
        // FIXME: because of accessoryView here is hack to identify only accessoryView without the real keyboard
        if keyboardFrame.height < 200 {
            tableView.verticalScrollIndicatorInsets = UIEdgeInsets.zero
        } else {
            tableView.verticalScrollIndicatorInsets = UIEdgeInsets(top: tableView.verticalScrollIndicatorInsets.top,
                                                                   left: tableView.verticalScrollIndicatorInsets.left,
                                                                   bottom: keyboardFrame.height - tableView.safeAreaInsets.bottom - accessoryViewHeight,
                                                                   right: tableView.verticalScrollIndicatorInsets.right)
        }

        tableView.contentInset = UIEdgeInsets(top: tableView.contentInset.top,
                                              left: tableView.contentInset.left,
                                              bottom: keyboardFrame.height - tableView.safeAreaInsets.bottom,
                                              right: tableView.contentInset.right)
        
        //FIXME: need to be improved, because tableview scroll animation and keyboard show animation should be more synchronous
        // and also there are glitches if hide keyboard by selecting the new cell
        if selectedCellWasVisible {
            tableView.scrollToNearestSelectedRow(at: .middle, animated: true)
        }
    }
    
    private var textChangedWorkItem: DispatchWorkItem?
    @IBAction private func textChanged(_ sender: UITextField) {
        
        textChangedWorkItem?.cancel()
        guard let text = sender.text, !text.isEmpty else {
            dataService.cancelSearch()
            updateState(.noDestination)
            return
        }
        
        let workItem = DispatchWorkItem {
            let searchWorkItem = self.dataService.searchDestination(query: text) { result in
                DispatchQueue.main.async {
                    switch result {
                    case .success(let destinations):
                        self.updateState(destinations.isEmpty ? .noResults : .results(destinations: destinations))
                    case .failure(.noInternetConnection):
                        self.updateState(.noInternetConnection)
                    default:
                        self.updateState(.noResults)
                        break
                    }
                }
            }
            switch searchWorkItem.wait(timeout: .now() + 0.5) {
            case .timedOut:
                DispatchQueue.main.async { self.updateState(.loading) }
            default:
                break
            }
        }
        textChangedWorkItem = workItem
        DispatchQueue.global(qos: .userInteractive).asyncAfter(deadline: .now() + 0.3, execute: workItem)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let flightVC = segue.destination as? FlightViewController {
            flightVC.route = Route(from: dataService.departure, to: selectedDestination!)
        }
    }
    
    override var canBecomeFirstResponder: Bool {
        return true
    }
    
    override var inputAccessoryView: UIView? {
        return accessoryView
    }
}

// MARK: - TableView delegate and data source

extension SearchViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        dataSourceDestinations.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let destinationCell = tableView.dequeueReusableCell(withIdentifier: "DestinationCell", for: indexPath) as? DestinationCell else {
            return UITableViewCell(style: .default, reuseIdentifier: nil)
        }
        
        let destination = dataSourceDestinations[indexPath.row]
        destinationCell.setup(withDestination: destination)
        return destinationCell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        updateState(.selected(destination: dataSourceDestinations[indexPath.row]))
    }
}
