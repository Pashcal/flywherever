//
//  UIKit+Animate.swift
//  FlyWherever
//
//  Created by Pavel Chupryna + on 17.04.2020.
//  Copyright © 2020 pashcal. All rights reserved.
//

import UIKit

extension UIView {
    func animateHidden(_ isHidden: Bool, duration: TimeInterval = 0.23, completion: (() -> Void)? = nil) {
        guard self.isHidden != isHidden else {
            completion?()
            return
        }
        
        if isHidden {
            let alpha = self.alpha
            UIView.animate(withDuration: duration, animations: { self.alpha = 0 }) { finished in
                if finished {
                    self.isHidden = true
                    self.alpha = alpha
                    completion?()
                }
            }
        } else {
            let alpha = self.alpha
            self.alpha = 0
            self.isHidden = false
            UIView.animate(withDuration: duration, animations: { self.alpha = alpha }) { finished in
                if finished {
                    completion?()
                }
            }
        }
    }
    
    func animateAlpha(_ value: CGFloat, duration: TimeInterval = 0.23, completion: (() -> Void)? = nil) {
        UIView.animate(withDuration: duration, animations: { self.alpha = value }) { finished in
            if finished {
                completion?()
            }
        }
    }
}

extension UILabel {
    func animateText(_ text: String, completion: (() -> Void)? = nil) {
        self.animateHidden(true, duration: 0.05) {
            self.text = text
            self.animateHidden(false) {
                completion?()
            }
        }
    }
}

extension UITableView {
    func animateReloadData(completion: (() -> Void)? = nil) {
        self.animateHidden(true) {
            self.reloadData()
            self.animateHidden(false) {
                completion?()
            }
        }
    }
}
