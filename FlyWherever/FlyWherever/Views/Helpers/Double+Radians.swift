//
//  Double+Radians.swift
//  FlyWherever
//
//  Created by Pavel Chupryna + on 19.04.2020.
//  Copyright © 2020 pashcal. All rights reserved.
//

import Foundation

extension Double {

    /// Radians to degrees
    func toDegrees() -> Double {
        return self * 180 / .pi
    }
    
    /// Degrees to radians
    func toRadians() -> Double {
        return self * .pi / 180
    }
}
