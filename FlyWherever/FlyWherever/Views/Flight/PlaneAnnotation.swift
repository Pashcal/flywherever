//
//  PlaneAnnotation.swift
//  FlyWherever
//
//  Created by Pavel Chupryna + on 19.04.2020.
//  Copyright © 2020 pashcal. All rights reserved.
//

import Foundation
import MapKit

class PlaneAnnotation: MKPointAnnotation {
    
    weak var annotationView: MKAnnotationView?
    
    private lazy var correction: CGFloat = {
        return AppConfig.isEmojiPlane ? .pi / 4 : .pi / 2
    }()
    
    var directionDegree: Double = 0 {
        didSet {
            let rotationRadians = AppConfig.isFun
                ? CGFloat(directionDegree * 3)
                : CGFloat(directionDegree.toRadians()) - correction
            annotationView?.transform = CGAffineTransform(rotationAngle: rotationRadians)
        }
    }
}
