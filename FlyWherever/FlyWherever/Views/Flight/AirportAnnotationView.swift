//
//  AirportAnnotationView.swift
//  FlyWherever
//
//  Created by Pavel Chupryna + on 19.04.2020.
//  Copyright © 2020 pashcal. All rights reserved.
//

import Foundation
import MapKit

class AirportAnnotationView: MKAnnotationView {
    
    private var airportCodeView: AirportCodeView?
    
    var code: String = "" {
        didSet {
            airportCodeView?.code = code
        }
    }
    
    override init(annotation: MKAnnotation?, reuseIdentifier: String?) {
        super.init(annotation: annotation, reuseIdentifier: reuseIdentifier)
        
        guard let annotation = annotation else {
            return
        }
        
        airportCodeView = Bundle.main.loadNibNamed("AirportCodeView", owner: self, options: nil)?.first! as? AirportCodeView
        
        if let codeView = airportCodeView {
            codeView.code = annotation.title!!
            codeView.layer.borderColor = UIColor.systemTeal.cgColor
            codeView.frame = CGRect(x: -codeView.bounds.width / 2,
                                    y: -codeView.bounds.height / 2,
                                    width: codeView.bounds.width,
                                    height: codeView.bounds.height)
            self.addSubview(codeView)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}
