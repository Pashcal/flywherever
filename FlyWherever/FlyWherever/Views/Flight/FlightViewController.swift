//
//  FlightViewController.swift
//  FlyWherever
//
//  Created by Pavel Chupryna + on 18.04.2020.
//  Copyright © 2020 pashcal. All rights reserved.
//

import UIKit
import MapKit

class FlightViewController: UIViewController {
    
    private let notificationFeedbackGenerator = UINotificationFeedbackGenerator()
    
    private var planeCurrentPoint = 0
    private var planePolyline: MKPolyline!
    private var planeAnnotation: PlaneAnnotation!
    
    private var didDisappear: Bool = false
    
    @IBOutlet private weak var closeBackgroundView: UIView!
    @IBOutlet private weak var mapView: MKMapView!
    
    let airportAnnotationIdentifier = "AirportAnnotation"
    let planeAnnotationIdentifier = "PlaneAnnotation"
    
    var route: Route!
    
    private func addRouteOverlay() {
        
        let from = route.from.location.mapPoint
        let to = route.to.location.mapPoint
        
        let maxLat = max(from.coordinate.latitude, to.coordinate.latitude)
        let maxLon = max(from.coordinate.longitude, to.coordinate.longitude)
        let minLat = min(from.coordinate.latitude, to.coordinate.latitude)
        let minLon = min(from.coordinate.longitude, to.coordinate.longitude)
        
        // Annotations
        
        let fromAnnotation = MKPointAnnotation()
        fromAnnotation.title = route.from.code
        fromAnnotation.coordinate = from.coordinate
        
        let toAnnotation = MKPointAnnotation()
        toAnnotation.title = route.to.code
        toAnnotation.coordinate = to.coordinate
        
        planeAnnotation = PlaneAnnotation()
        planeAnnotation.coordinate = from.coordinate
        
        mapView.addAnnotations([planeAnnotation, fromAnnotation, toAnnotation])
        
        // Path

        if AppConfig.isGeodesicFlight {
            var points = [from, to]
            planePolyline = MKGeodesicPolyline(points: &points, count: 2)
        } else {
            // TODO
        }
        mapView.addOverlay(planePolyline)
        
        // Zoom region
        
        let spanCoefficient = 1.3
        let span = MKCoordinateSpan(latitudeDelta: (maxLat - minLat) * spanCoefficient,
                                    longitudeDelta: (maxLon - minLon) * spanCoefficient)
        let region = MKCoordinateRegion(center: from.middle(to).coordinate, span: span)
        mapView.setRegion(region, animated: true)
        
        // FIXME: Incorrect span for too long path (e.g. Tokyo), but probably it because of screen size
    }
    
    @objc private func animateFlight() {
        
        guard planeCurrentPoint < planePolyline.pointCount - 1 else {
            notificationFeedbackGenerator.prepare()
            notificationFeedbackGenerator.notificationOccurred(.success)
            return
        }
        
        guard !didDisappear else {
            return
        }
        
        let points = planePolyline.points()
        let step = Int(planePolyline.pointCount / 500)
        
        let fromPoint = points[planeCurrentPoint]
        planeCurrentPoint = min(planeCurrentPoint + step, planePolyline.pointCount - 1)
        let toPoint = points[planeCurrentPoint]
        
        planeAnnotation.directionDegree = fromPoint.directionTo(toPoint)
        planeAnnotation.coordinate = toPoint.coordinate
        
        perform(#selector(animateFlight), with: nil, afterDelay: 0.02)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        AppConfig.isFun = false
        
        let mask = CAShapeLayer()
        mask.path = UIBezierPath(roundedRect: closeBackgroundView.bounds, byRoundingCorners: .bottomLeft, cornerRadii: CGSize(width: 16, height: 16)).cgPath
        closeBackgroundView.layer.mask = mask
        
        addRouteOverlay()
        planeCurrentPoint = 0
        animateFlight()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        didDisappear = true
    }
    
    @IBAction private func closeAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}

extension Location {
    var mapPoint: MKMapPoint {
        return MKMapPoint(CLLocationCoordinate2D(latitude: latitude, longitude: longitude))
    }
}

extension MKMapPoint {
    func middle(_ to: MKMapPoint) -> MKMapPoint {
        let middle = MKMapPoint(x: (self.x + to.x) / 2,
                                y: (self.y + to.y) / 2)
        return middle
    }
    
    func directionTo(_ mapPoint: MKMapPoint) -> CLLocationDirection {
        let x = mapPoint.x - self.x
        let y = mapPoint.y - self.y
        return atan2(y, x).toDegrees().truncatingRemainder(dividingBy: 360) + 90
    }
}
