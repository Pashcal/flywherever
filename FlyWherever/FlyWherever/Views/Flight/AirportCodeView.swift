//
//  AirportCodeView.swift
//  FlyWherever
//
//  Created by Pavel Chupryna + on 19.04.2020.
//  Copyright © 2020 pashcal. All rights reserved.
//

import UIKit
import MapKit

class AirportCodeView: UIView {
    
    @IBOutlet private weak var codeLabel: UILabel!
    
    var code: String = "" {
        didSet {
            codeLabel.text = code
        }
    }
}
