//
//  FlightViewController+MKMapViewDelegate.swift
//  FlyWherever
//
//  Created by Pavel Chupryna + on 19.04.2020.
//  Copyright © 2020 pashcal. All rights reserved.
//

import Foundation
import MapKit

extension FlightViewController: MKMapViewDelegate {
    
    private func airportAnnotationView(_ mapView: MKMapView, _ annotation: MKAnnotation) -> MKAnnotationView {
        
        var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: airportAnnotationIdentifier) as? AirportAnnotationView
        if annotationView == nil {
            annotationView = AirportAnnotationView(annotation: annotation, reuseIdentifier: airportAnnotationIdentifier)
        }
        
        annotationView?.annotation = annotation
        annotationView?.code = annotation.title!!
        
        return annotationView ?? MKAnnotationView()
    }
    
    private func planeAnnotationView(_ mapView: MKMapView, _ annotation: PlaneAnnotation) -> MKAnnotationView {
        
        var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: planeAnnotationIdentifier)
        if annotationView == nil {
            annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: planeAnnotationIdentifier)
        }
        
        guard let planeView = annotationView else {
            return MKAnnotationView()
        }
        
        planeView.annotation = annotation
        if AppConfig.isEmojiPlane {
            planeView.frame = CGRect(x: 0, y: 0, width: 42, height: 42)
            let emojiLabel = UILabel(frame: planeView.frame)
            emojiLabel.text = "✈️"
            emojiLabel.font = UIFont.systemFont(ofSize: 36)
            planeView.addSubview(emojiLabel)
        } else {
            planeView.image = UIImage(named: "plane")
        }
        annotation.annotationView = planeView
        
        // FIXME: ¯\_(ツ)_/¯ Randomly plane is became under airport
        // planeView.superview?.bringSubviewToFront(planeView) doesn't help
        
        return planeView
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        guard annotation is MKPointAnnotation else {
            return nil
        }
        
        switch annotation {
        case let planeAnnotation as PlaneAnnotation:
            return planeAnnotationView(mapView, planeAnnotation)
        default:
            return airportAnnotationView(mapView, annotation)
        }
    }
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        
        if overlay is MKPolyline {
            let polylineRenderer = MKPolylineRenderer(overlay: overlay)
            polylineRenderer.strokeColor = UIColor.systemTeal
            polylineRenderer.lineDashPattern = [2, 6]
            polylineRenderer.lineWidth = 3
            polylineRenderer.lineJoin = .round
            return polylineRenderer
        }
        
        return MKPolylineRenderer()
    }
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        guard view.annotation is PlaneAnnotation else {
            return
        }
        
        AppConfig.isFun = !AppConfig.isFun
        mapView.deselectAnnotation(view.annotation, animated: false) //Hack for select one annotation twice
    }
}
