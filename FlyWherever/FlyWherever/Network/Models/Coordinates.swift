//
//  Coordinates.swift
//  FlyWherever
//
//  Created by Pavel Chupryna + on 18.04.2020.
//  Copyright © 2020 pashcal. All rights reserved.
//

import Foundation

struct Coordinates: Codable {
    var lat: Double
    var lon: Double
}
