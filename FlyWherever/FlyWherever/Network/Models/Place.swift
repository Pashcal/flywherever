//
//  Place.swift
//  FlyWherever
//
//  Created by Pavel Chupryna + on 18.04.2020.
//  Copyright © 2020 pashcal. All rights reserved.
//

import Foundation

struct Place: Codable {
    var iata: String
    var name: String
    var airportName: String?
    var location: Coordinates
    
    enum CodingKeys: String, CodingKey {
        case iata
        case name
        case airportName = "airport_name"
        case location
    }
}
