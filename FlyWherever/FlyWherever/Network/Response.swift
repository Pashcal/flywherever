//
//  Response.swift
//  FlyWherever
//
//  Created by Pavel Chupryna + on 18.04.2020.
//  Copyright © 2020 pashcal. All rights reserved.
//

import Foundation

enum Response<T: Codable> {
    case success(T)
    case failure(FlyError)
}
