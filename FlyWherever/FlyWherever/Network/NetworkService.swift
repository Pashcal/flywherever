//
//  NetworkService.swift
//  FlyWherever
//
//  Created by Pavel Chupryna + on 18.04.2020.
//  Copyright © 2020 pashcal. All rights reserved.
//

import Foundation

class NetworkService {
    
    private let baseUrl = "http://places.aviasales.ru"
    
    func getPlaces(term: String, locale: String, completion: @escaping (Response<[Place]>) -> Void) -> URLSessionDataTask {
        var urlComponents = URLComponents(string: baseUrl)!
        urlComponents.path = "/places"
        urlComponents.queryItems = [
            URLQueryItem(name: "term", value: term),
            URLQueryItem(name: "locale", value: locale)
        ]
        let request = URLRequest(url: urlComponents.url!, cachePolicy: .reloadIgnoringLocalCacheData, timeoutInterval: 30)
        let config = URLSessionConfiguration.default
        
//        config.waitsForConnectivity = true
//        request.cachePolicy = URLRequest.CachePolicy.returnCacheDataElseLoad
        
        let task = URLSession(configuration: config).dataTask(with: request) { data, response, error in
            guard let data = data else {
                if let error = error {
                    print("ERROR | Network: \(error.localizedDescription)")
                }
                // TODO: Can be repaced by reachability
                completion(Response<[Place]>.failure(FlyError.noInternetConnection))
                return
            }
            
            do {
                let result = try JSONDecoder().decode([Place].self, from: data)
                completion(Response<[Place]>.success(result))
            } catch let error {
                completion(Response<[Place]>.failure(FlyError.another(error)))
            }
        }
        task.resume()
        return task
    }
}
